package RunScripts;

import Globales.*;
import TestPages.*;
import TestPages.BancaVirtual.*;
import org.junit.*;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_013_CYBERBANK-4822
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_013_CYBERBANK-4822

public class RunCheckAllMenuOtros {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("RunCheckIngresosMenuOtros");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 100 (Official Build) (64-bit)");

        Login login = new Login();
        login.Ingresar("8");

        PreguntaSeguridad pregunta = new PreguntaSeguridad();
        pregunta.ResponderCorrectamente();

        RegistroEquipo equipo = new RegistroEquipo();
        equipo.vp_etiqueta_nomatriculado();
        equipo.click_continuar();

        PosicionConsolidada posicion = new PosicionConsolidada();
        posicion.vp_etiqueta_saludo();

    }

    @Test
    public void run_menuPrincipalOtros() throws InterruptedException {
        MenuPrincipalOtros menuPrincipalOtros = new MenuPrincipalOtros();

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosFacturacion("Facturaci");
        menuPrincipalOtros.validate_EtiquetaMenu("Facturaci");

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosBuro("bur");
        menuPrincipalOtros.validate_EtiquetaMenu("bur");

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosScore("score");
        menuPrincipalOtros.validate_EtiquetaMenu("Score");

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosTransacciones("transacciones");
        menuPrincipalOtros.validate_EtiquetaMenu("transacciones");

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosConsulta("Consulta");
        menuPrincipalOtros.validate_EtiquetaMenu("Consulta");

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosExpress("express");
        menuPrincipalOtros.validate_EtiquetaMenu("express");

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosSwift("Swift");
        menuPrincipalOtros.validate_EtiquetaMenu("Swift");

        menuPrincipalOtros.click_btnMenuPrincipalOtros("Otros");
        menuPrincipalOtros.click_btnFncOtrosProgramaciones("Programaciones");
        menuPrincipalOtros.validate_EtiquetaMenu("Programaciones");
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }

}
