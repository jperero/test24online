package RunScripts;

import Globales.*;
import TestPages.*;
import TestPages.BancaVirtual.*;
import org.junit.*;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_007_CYBERBANK-4678
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_007_CYBERBANK-4678

public class RunCheckAllMenuTransferir {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("RunCheckIngresosMenuTransferir");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 90.0.4324.190 (Official Build) (64-bit)");

        Login login = new Login();
        login.Ingresar("8");

        PreguntaSeguridad pregunta = new PreguntaSeguridad();
        pregunta.ResponderCorrectamente();

        RegistroEquipo equipo = new RegistroEquipo();
        equipo.vp_etiqueta_nomatriculado();
        equipo.click_continuar();

        PosicionConsolidada posicion = new PosicionConsolidada();
        posicion.vp_etiqueta_saludo();

    }

    @Test
    public void run_menuPrincipalTransferir() throws InterruptedException {
        MenuPrincipalTransferir menuPrincipalTransferir = new MenuPrincipalTransferir();
        MenuTransferirCuentas menuTransferirCuentas = new MenuTransferirCuentas();
        MenuTransferirMatricular menuTransferirMatricular = new MenuTransferirMatricular();

        menuPrincipalTransferir.click_btnMenuPrincipalTransferir("Transferir");
        menuTransferirCuentas.click_btnFncTransferirCuentasPropias("propias");
        menuTransferirCuentas.validate_EtiquetaMenu("propias");

        menuPrincipalTransferir.click_btnMenuPrincipalTransferir("Transferir");
        menuTransferirCuentas.click_btnFncTransferirCuentasTerceros("Terceros");
        menuTransferirCuentas.validate_EtiquetaMenu("Terceros");

        menuPrincipalTransferir.click_btnMenuPrincipalTransferir("Transferir");
        menuTransferirCuentas.click_btnFncTransferirCuentasInternacionales("Internacionales");
        menuTransferirCuentas.validate_EtiquetaMenu("Internacionales");

        menuPrincipalTransferir.click_btnMenuPrincipalTransferir("Transferir");
        menuTransferirMatricular.click_btnFncTransferirMatricularTercerosNew("terceros");
        menuTransferirMatricular.validate_EtiquetaMenu("matriculaciones");

        menuPrincipalTransferir.click_btnMenuPrincipalTransferir("Transferir");
        menuTransferirMatricular.click_btnFncTransferirMatricularExterior("exterior");
        menuTransferirMatricular.validate_EtiquetaMenu("exterior");

        menuPrincipalTransferir.click_btnMenuPrincipalTransferir("Transferir");
        menuTransferirMatricular.click_btnFncTransferirMatricularExteriorNew("exterior");
        menuTransferirMatricular.validate_EtiquetaMenu("matriculaciones");

        menuPrincipalTransferir.click_btnMenuPrincipalTransferir("Transferir");
        menuTransferirMatricular.click_btnFncTransferirMatricularDonaciones("Donaciones");
        menuTransferirMatricular.validate_EtiquetaMenu("Donaciones");
    }

    @Test
    public void run_menuPrincipalPagar() throws InterruptedException {
        MenuPrincipalPagar menuPrincipalPagar = new MenuPrincipalPagar();

        menuPrincipalPagar.click_btnMenuPrincipalPagar("Pagar");
        menuPrincipalPagar.click_btnFncPagarServicios("Pagar");
        menuPrincipalPagar.validate_EtiquetaMenu("pagos");
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }

}
