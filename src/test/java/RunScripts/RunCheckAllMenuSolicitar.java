package RunScripts;

import Globales.*;
import TestPages.*;
import TestPages.BancaVirtual.*;
import org.junit.*;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_011_CYBERBANK-4821
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_011_CYBERBANK-4821

public class RunCheckAllMenuSolicitar {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("RunCheckIngresosMenuSolicitar");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 90.0.4324.190 (Official Build) (64-bit)");

        Login login = new Login();
        login.Ingresar("14");

        PreguntaSeguridad pregunta = new PreguntaSeguridad();
        pregunta.ResponderCorrectamente();

        RegistroEquipo equipo = new RegistroEquipo();
        equipo.vp_etiqueta_nomatriculado();
        equipo.click_continuar();

        PosicionConsolidada posicion = new PosicionConsolidada();
        posicion.vp_etiqueta_saludo();

    }

    @Test
    public void run_menuPrincipalSolicitar() throws InterruptedException {
        MenuPrincipalSolicitar menuPrincipalSolicitar = new MenuPrincipalSolicitar();
        MenuSolicitarServicios menuSolicitarServicios = new MenuSolicitarServicios();
        MenuSolicitarChequeras menuSolicitarChequeras = new MenuSolicitarChequeras();
        MenuSolicitarProductos menuSolicitarProductos = new MenuSolicitarProductos();

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarServicios.click_btnFncSolicitarServiciosAvisos("Avisos24");
        menuSolicitarServicios.validate_EtiquetaMenu("Avisos24");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarServicios.click_btnFncSolicitarServiciosMovil("24m");
        menuSolicitarServicios.validate_EtiquetaMenu("24m");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarServicios.click_btnFncSolicitarServiciosComprobantes("Comprobantes");
        menuSolicitarServicios.validate_EtiquetaMenu("Comprobantes");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarServicios.click_btnFncSolicitarServiciosDeposito("sito");
        menuSolicitarServicios.validate_EtiquetaMenu("sito");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarServicios.click_btnFncSolicitarServiciosCompras("compras");
        menuSolicitarServicios.validate_EtiquetaMenu("compras");


        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarChequeras.click_btnFncSolicitarChequerasSolicitar ("Solicitar");
        menuSolicitarChequeras.validate_EtiquetaMenu("Solicitud");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarChequeras.click_btnFncSolicitarChequerasAnular ("Anular");
        menuSolicitarChequeras.validate_EtiquetaMenu("Anular");


        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarProductos.click_btnFncSolicitarProductosReferencias ("Referencias");
        menuSolicitarProductos.validate_EtiquetaMenu("Referencias");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarProductos.click_btnFncSolicitarProductosCtaMas ("Cuenta");
        menuSolicitarProductos.validate_EtiquetaMenu("Cuenta");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarProductos.click_btnFncSolicitarProductosOnbSolicitud ("Certificado");
        menuSolicitarProductos.validate_EtiquetaMenu("Certificado");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarProductos.click_btnFncSolicitarProductosForm ("Formularios");
        menuSolicitarProductos.validate_EtiquetaMenu("Producto");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarProductos.click_btnFncSolicitarProductosFormSolicitud ("Consulta");
        menuSolicitarProductos.validate_EtiquetaMenu("Consulta");

        menuPrincipalSolicitar.click_btnMenuPrincipalSolicitar("Solicitar");
        menuSolicitarProductos.click_btnFncSolicitarProductosCredimax ("Credimax");
        menuSolicitarProductos.validate_EtiquetaMenu("Credimax");

    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }

}
