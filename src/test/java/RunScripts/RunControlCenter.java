package RunScripts;

import Globales.*;
import TestPages.*;
import org.junit.*;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_007_CYBERBANK-4678
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_007_CYBERBANK-4678

public class RunControlCenter {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("RunCheckAllMenuTransferir");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 90.0.4324.190 (Official Build) (64-bit)");

    }

    @Test
    public void run_Login() throws InterruptedException {
        TestControlCenter testControlCenter = new TestControlCenter();

        testControlCenter.click_btnControlCenterLogin("ralava", "P@ssw0rd");
        testControlCenter.click_btnEntrarApps();

    }


    @After
    public void salir()
    {
        Reporte.finReporte();
    }

}
