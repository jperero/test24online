package RunScripts;

import Globales.*;
import TestPages.*;
import TestPages.BancaVirtual.*;
import org.junit.*;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_007_CYBERBANK-4678
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_007_CYBERBANK-4678

public class RunCheckAllMenuPerfil {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("RunCheckIngresosMenuPerfil");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 100 (Official Build) (64-bit)");

        Login login = new Login();
        login.Ingresar("8");

        PreguntaSeguridad pregunta = new PreguntaSeguridad();
        pregunta.ResponderCorrectamente();

        RegistroEquipo equipo = new RegistroEquipo();
        equipo.vp_etiqueta_nomatriculado();
        equipo.click_continuar();

        PosicionConsolidada posicion = new PosicionConsolidada();
        posicion.vp_etiqueta_saludo();

    }

    @Test
    public void run_menuPrincipalPerfil() throws InterruptedException {
        MenuPrincipalPerfil menuPrincipalPerfil = new MenuPrincipalPerfil();

        menuPrincipalPerfil.click_btnMenuPrincipalPerfil("Mi perfil");
        menuPrincipalPerfil.click_btnFncPerfilConfiguracion("de perfil");
        menuPrincipalPerfil.validate_EtiquetaMenu("de perfil");

        menuPrincipalPerfil.click_btnMenuPrincipalPerfil("Mi perfil");
        menuPrincipalPerfil.click_btnFncPerfilActualizacion("personales");
        menuPrincipalPerfil.validate_EtiquetaMenu("personales");

        menuPrincipalPerfil.click_btnMenuPrincipalPerfil("Mi perfil");
        menuPrincipalPerfil.click_btnFncPerfilAdministracion("equipos");
        menuPrincipalPerfil.validate_EtiquetaMenu("equipos");

        menuPrincipalPerfil.click_btnMenuPrincipalPerfil("Mi perfil");
        menuPrincipalPerfil.click_btnFncPerfilConsulta("Consulta");
        menuPrincipalPerfil.validate_EtiquetaMenu("Consulta");

        menuPrincipalPerfil.click_btnMenuPrincipalPerfil("Mi perfil");
        menuPrincipalPerfil.click_btnFncPerfilDispositivo("Dispositivo");
        menuPrincipalPerfil.validate_EtiquetaMenu("dispositivo");

    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }

}
