package RunScripts;

import Globales.*;
import TestPages.*;
import TestPages.BancaVirtual.*;
import org.junit.*;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_007_CYBERBANK-4678
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_007_CYBERBANK-4678

public class RunCheckAllMenuInversiones {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("RunCheckIngresosMenuInversiones");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 90.0.4324.190 (Official Build) (64-bit)");

        Login login = new Login();
        login.Ingresar("8");

        PreguntaSeguridad pregunta = new PreguntaSeguridad();
        pregunta.ResponderCorrectamente();

        RegistroEquipo equipo = new RegistroEquipo();
        equipo.vp_etiqueta_nomatriculado();
        equipo.click_continuar();

        PosicionConsolidada posicion = new PosicionConsolidada();
        posicion.vp_etiqueta_saludo();

    }

    @Test
    public void run_menuPrincipalInversiones() throws InterruptedException {
        MenuPrincipalInversiones menuPrincipalInversiones = new MenuPrincipalInversiones();

        menuPrincipalInversiones.click_btnMenuPrincipalInversiones("Inversiones");
        menuPrincipalInversiones.click_btnFncInversionesDepositos("certificados");
        menuPrincipalInversiones.validate_EtiquetaMenu("certificados");

        menuPrincipalInversiones.click_btnMenuPrincipalInversiones("Inversiones");
        menuPrincipalInversiones.click_btnFncInversionesTasas("tasas");
        menuPrincipalInversiones.validate_EtiquetaMenu("tasas");

        menuPrincipalInversiones.click_btnMenuPrincipalInversiones("Inversiones");
        menuPrincipalInversiones.click_btnFncInversionesOnboarding("online");
        menuPrincipalInversiones.validate_EtiquetaMenu("online");

    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }

}
