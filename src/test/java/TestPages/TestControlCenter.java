package TestPages;

import Globales.Util;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_009_CYBERBANK-4818
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_009_CYBERBANK-4818

public class TestControlCenter {

    //Variables Generales
    String vGral_menu = "Menu Principal ";
    String vGral_detalleMenu = "Verificacion de etiqueta de menu ";

    @FindBy(id="login_textField0")
    WebElement vwe_txtLogin = null;

    @FindBy(id="actionButtonIngresar")
    WebElement vwe_btnContinuar = null;

    @FindBy(id="login_textField1")
    WebElement vwe_txtPass = null;

    @FindBy(className="buttonAppLauncher")
    WebElement vwe_btnApps = null;

    @FindBy(className="lblApps",name = "Reglas")
    WebElement vwe_btnAppReglas = null;

    public TestControlCenter() {
        PageFactory.initElements(Util.driver, this);
    }

    public void click_btnControlCenterLogin(String user, String pass) {
        try {
            String textVwe = vwe_txtLogin.getText();
            Thread.sleep(2500);
            vwe_txtLogin.sendKeys(user);
            vwe_btnContinuar.click();
            vwe_txtPass.sendKeys(pass);
            vwe_btnContinuar.click();

            Util.assert_contiene(vGral_menu,  vGral_detalleMenu, textVwe, user, false, "N");
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            e.printStackTrace();
        }
    }

    public void click_btnEntrarApps() {
        try {
            String textVwe = vwe_btnApps.getText();
            Thread.sleep(2500);
            vwe_btnApps.click();
            vwe_btnAppReglas.click();

            Util.assert_contiene(vGral_menu,  vGral_detalleMenu, textVwe, textVwe, false, "N");
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            e.printStackTrace();
        }
    }


}
