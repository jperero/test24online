package TestPages.BancaVirtual;

import Globales.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_011_CYBERBANK-4821
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_011_CYBERBANK-4821

public class MenuSolicitarProductos {

    public MenuSolicitarProductos() {
        PageFactory.initElements(Util.driver, this);
    }

    //Variables Generales
    String vGral_menu = "MENU SOLICITAR PRODUCTOS";
    String vGral_err = "ERR: Hay un error en ";
    String vGral_msjReporte = "Verificacion de etiqueta de menu";
    int vGral_sleep = 1500;

    //Variables Especificas
    @FindBy(id="EB_CERTIFICADO_BANCARIO")
    WebElement vwe_btnFncSolicitarProductosReferencias = null;

    @FindBy(id="EB_CUENTA_MAS_ONLINE")
    WebElement vwe_btnFncSolicitarProductosCtaMas = null;

    @FindBy(id="EB_INVERSIONES_ONBORDING_SOLICITUD")
    WebElement vwe_btnFncSolicitarProductosOnbSolicitud = null;

    @FindBy(id="EB_FORMULARIOS_INTELIGENTES")
    WebElement vwe_btnFncSolicitarProductosForm = null;

    @FindBy(id="EB_CONSULTA_DE_SOLICITUD")
    WebElement vwe_btnFncSolicitarProductosFormSolicitud = null;

    @FindBy(id="EB_CREDIMAX_ONLINE")
    WebElement vwe_btnFncSolicitarProductosCredimax = null;

    public void click_btnFncSolicitarProductosReferencias(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarProductosReferencias.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarProductosReferencias.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarProductosCtaMas(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarProductosCtaMas.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarProductosCtaMas.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarProductosOnbSolicitud(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarProductosOnbSolicitud.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarProductosOnbSolicitud.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarProductosForm(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarProductosForm.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarProductosForm.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarProductosFormSolicitud(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarProductosFormSolicitud.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarProductosFormSolicitud.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarProductosCredimax(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarProductosCredimax.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarProductosCredimax.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void validate_EtiquetaMenu(String tituloSeccion) {
        try {
            WebElement vwe_lblTituloSeccion = Util.driver.findElement(By.xpath("//div[@class='titulo-seccion' and contains(., '"+ tituloSeccion + "')]"));
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_lblTituloSeccion.getText(), tituloSeccion, true, "N");
        } catch (NoSuchElementException e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    private void validate_Error(String error, String tituloSeccion) {
        System.out.println(vGral_err + tituloSeccion);
        System.out.println(error);
        Util.assert_contiene("Menu " + vGral_menu, "ERROR NO SE COMPROBO LA ETIQUETA DEL MENU", tituloSeccion, tituloSeccion, false, "C");
    }

}
