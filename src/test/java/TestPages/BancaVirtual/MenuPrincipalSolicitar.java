package TestPages.BancaVirtual;

import Globales.Util;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_011_CYBERBANK-4821
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_011_CYBERBANK-4821

public class MenuPrincipalSolicitar {

    @FindBy(id="SOLICITUDES")
    WebElement vwe_btnMenuPrincipalSolicitar = null;

    //Variables Generales
    String vGral_menu = "Menu Principal ";
    String vGral_detalleMenu = "Verificacion de etiqueta de menu ";

    public MenuPrincipalSolicitar() {
        PageFactory.initElements(Util.driver, this);
    }

    public void click_btnMenuPrincipalSolicitar(String tituloMenu) {
        try {
            String textVwe = vwe_btnMenuPrincipalSolicitar.getText();
            Thread.sleep(1000);
            vwe_btnMenuPrincipalSolicitar.click();
            Util.assert_contiene(vGral_menu+tituloMenu,  vGral_detalleMenu, textVwe, tituloMenu, false, "N");
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            e.printStackTrace();
        }
    }

}
