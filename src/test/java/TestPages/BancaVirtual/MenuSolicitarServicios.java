package TestPages.BancaVirtual;

import Globales.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_011_CYBERBANK-4821
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_011_CYBERBANK-4821

public class MenuSolicitarServicios {

    public MenuSolicitarServicios() {
        PageFactory.initElements(Util.driver, this);
    }

    //Variables Generales
    String vGral_menu = "MENU SOLICITAR SERVICIOS";
    String vGral_err = "ERR: Hay un error en ";
    String vGral_msjReporte = "Verificacion de etiqueta de menu";
    int vGral_sleep = 1000;

    //Variables Especificas
    @FindBy(id="EB_SUSCRIPCIONES_PLANES")
    WebElement vwe_btnFncSolicitarServiciosAvisos = null;

    @FindBy(id="EB_ACTIVACION_DESACTIVACION_24MOVIL")
    WebElement vwe_btnFncSolicitarServiciosMovil = null;

    @FindBy(id="EB_COMPROBANTES_DE_CREDITO")
    WebElement vwe_btnFncSolicitarServiciosComprobantes = null;

    @FindBy(id="EB_ACTIVACION_DE_DEPOSITO_EXPRESS")
    WebElement vwe_btnFncSolicitarServiciosDeposito = null;

    @FindBy(id="EB_BOTON_DE_PAGOS")
    WebElement vwe_btnFncSolicitarServiciosCompras = null;

    @FindBy(id="EB_QUICKPAY_RETIRO_SIN_TARJETA_PROPIO")
    WebElement vwe_btnFncSolicitarServiciosRetiro = null;

    public void click_btnFncSolicitarServiciosAvisos(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarServiciosAvisos.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarServiciosAvisos.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarServiciosMovil(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarServiciosMovil.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarServiciosMovil.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarServiciosComprobantes(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarServiciosComprobantes.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarServiciosComprobantes.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarServiciosDeposito(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarServiciosDeposito.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarServiciosDeposito.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarServiciosCompras(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarServiciosCompras.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarServiciosCompras.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncSolicitarServiciosRetiro(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncSolicitarServiciosRetiro.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncSolicitarServiciosRetiro.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void validate_EtiquetaMenu(String tituloSeccion) {
        try {
            WebElement vwe_lblTituloSeccion = Util.driver.findElement(By.xpath("//div[@class='titulo-seccion' and contains(., '"+ tituloSeccion + "')]"));
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_lblTituloSeccion.getText(), tituloSeccion, true, "N");
        } catch (NoSuchElementException e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    private void validate_Error(String error, String tituloSeccion) {
        System.out.println(vGral_err + tituloSeccion);
        System.out.println(error);
        Util.assert_contiene("Menu " + vGral_menu, "ERROR NO SE COMPROBO LA ETIQUETA DEL MENU", tituloSeccion, tituloSeccion, false, "C");
    }

}
