package TestPages.BancaVirtual;

import Globales.*;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_012_CYBERBANK-4819
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_012_CYBERBANK-4819

public class MenuPrincipalPerfil {

    public MenuPrincipalPerfil() {
        PageFactory.initElements(Util.driver, this);
    }

    //Variables Generales
    String vGral_err = "ERR: Hay un error en ";
    String vGral_menu = "Menu Principal Perfil";
    String vGral_msjReporte = "Verificacion de etiqueta de menu";
    int vGral_sleep = 3000;

    @FindBy(id="menuMiPerfil")
    WebElement vwe_btnMenuPrincipalPerfil = null;

    @FindBy(id="menuOperador")
    WebElement vwe_btnMenuPrincipalPerfilOperador = null;

    @FindBy(xpath="//a[@href='/BOLI-ebanking/usuario/miPerfil.htm']")
    WebElement vwe_btnFncPerfilConfiguracion = null;

    @FindBy(xpath="//a[@href='/BOLI-ebanking/misDatos/datosPersonales/actualizacion.htm']")
    WebElement vwe_btnFncPerfilActualizacion = null;

    @FindBy(xpath="//a[@href='/BOLI-ebanking/administracionEquipos.htm']")
    WebElement vwe_btnFncPerfilAdministracion = null;

    @FindBy(xpath="//a[@href='/BOLI-ebanking/consultas/cupos.htm']")
    WebElement vwe_btnFncPerfilConsulta = null;

    @FindBy(xpath="//a[@href='/BOLI-ebanking/desbloqueoDispositivoSeguridad.htm']")
    WebElement vwe_btnFncPerfilDispositivo = null;

    public void click_btnMenuPrincipalPerfil(String tituloMenu) {
        try {
            vwe_btnMenuPrincipalPerfil.click();
            Thread.sleep(2500);
            String textVwe = vwe_btnMenuPrincipalPerfilOperador.getText();
            Util.assert_contiene(vGral_menu+tituloMenu,  vGral_msjReporte, textVwe, tituloMenu, false, "N");
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            e.printStackTrace();
        }
    }

    public void click_btnFncPerfilActualizacion(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncPerfilActualizacion.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncPerfilActualizacion.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncPerfilAdministracion(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncPerfilAdministracion.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncPerfilAdministracion.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncPerfilConsulta(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncPerfilConsulta.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncPerfilConsulta.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncPerfilDispositivo(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncPerfilDispositivo.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncPerfilDispositivo.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncPerfilConfiguracion(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncPerfilConfiguracion.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncPerfilConfiguracion.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void validate_EtiquetaMenu(String tituloSeccion) {
        try {
            WebElement vwe_lblTituloSeccion = Util.driver.findElement(By.xpath("//div[@class='titulo-seccion' and contains(., '"+ tituloSeccion + "')]"));
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_lblTituloSeccion.getText(), tituloSeccion, true, "N");
        } catch (NoSuchElementException e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    private void validate_Error(String error, String tituloSeccion) {
        System.out.println(vGral_err + tituloSeccion);
        System.out.println(error);
        Util.assert_contiene("Menu " + vGral_menu, "ERROR NO SE COMPROBO LA ETIQUETA DEL MENU", tituloSeccion, tituloSeccion, false, "C");
    }

}
