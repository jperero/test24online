package TestPages.BancaVirtual;

import Globales.*;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_008_CYBERBANK-4817
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_008_CYBERBANK-4817

public class MenuPrincipalInversiones {

    public MenuPrincipalInversiones() {
        PageFactory.initElements(Util.driver, this);
    }

    //Variables Generales
    String vGral_err = "ERR: Hay un error en ";
    String vGral_menu = "Menu Principal Inversiones";
    String vGral_msjReporte = "Verificacion de etiqueta de menu";
    int vGral_sleep = 3000;

    @FindBy(id="INVERSIONES")
    WebElement vwe_btnMenuPrincipalInversiones = null;

    @FindBy(id="EB_CONSULTA_DE_DEPOSITOS_A_PLAZO")
    WebElement vwe_btnFncInversionesDepositos = null;

    @FindBy(id="EB_CONSULTA_DE_TASAS_DE_DEPOSITO_A_PLAZO")
    WebElement vwe_btnFncInversionesTasas = null;

    @FindBy(id="EB_INVERSIONES_ONBORDING")
    WebElement vwe_btnFncInversionesOnboarding = null;

    public void click_btnMenuPrincipalInversiones(String tituloMenu) {
        try {
            String textVwe = vwe_btnMenuPrincipalInversiones.getText();
            Thread.sleep(2500);
            vwe_btnMenuPrincipalInversiones.click();
            Util.assert_contiene(vGral_menu+tituloMenu,  vGral_msjReporte, textVwe, tituloMenu, false, "N");
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            e.printStackTrace();
        }
    }

    public void click_btnFncInversionesDepositos(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncInversionesDepositos.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncInversionesDepositos.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncInversionesTasas(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncInversionesTasas.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncInversionesTasas.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncInversionesOnboarding(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncInversionesOnboarding.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncInversionesOnboarding.click();
            Thread.sleep(vGral_sleep);
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void validate_EtiquetaMenu(String tituloSeccion) {
        try {
            WebElement vwe_lblTituloSeccion = Util.driver.findElement(By.xpath("//div[@class='titulo-seccion' and contains(., '"+ tituloSeccion + "')]"));
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_lblTituloSeccion.getText(), tituloSeccion, true, "N");
        } catch (NoSuchElementException e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    private void validate_Error(String error, String tituloSeccion) {
        System.out.println(vGral_err + tituloSeccion);
        System.out.println(error);
        Util.assert_contiene("Menu " + vGral_menu, "ERROR NO SE COMPROBO LA ETIQUETA DEL MENU", tituloSeccion, tituloSeccion, false, "C");
    }

}
