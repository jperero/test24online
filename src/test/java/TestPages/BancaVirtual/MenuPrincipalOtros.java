package TestPages.BancaVirtual;

import Globales.*;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*Kevin Falcones - Senior Testing Automation
  Automatizacion de Validaciones Antes de Pase a Produccion - KFA_013_CYBERBANK-4822
  Condiciones: Validar Ingreso a cada una de las opciones del Canal
 */
//INI-->KFA_013_CYBERBANK-4822

public class MenuPrincipalOtros {

    public MenuPrincipalOtros() {
        PageFactory.initElements(Util.driver, this);
    }

    //Variables Generales
    String vGral_err = "ERR: Hay un error en ";
    String vGral_menu = "Menu Principal Otros";
    String vGral_msjReporte = "Verificacion de etiqueta de menu";
    int vGral_sleep = 2500;

    @FindBy(id="CONSULTAS")
    WebElement vwe_btnMenuPrincipalOtros = null;

    @FindBy(id="EB_FACTURACION_ELECTRONICA")
    WebElement vwe_btnFncOtrosFacturacion = null;

    @FindBy(id="EB_CREDIT_REPORT")
    WebElement vwe_btnFncOtrosBuro = null;

    @FindBy(id="EB_CONSULTA_SCORE_CREDITICIO")
    WebElement vwe_btnFncOtrosScore = null;

    @FindBy(id="EB_CONSULTA_TRANSACCIONES")
    WebElement vwe_btnFncOtrosTransacciones = null;

    @FindBy(id="EB_CONSULTA_PROGRAMADAS1")
    WebElement vwe_btnFncOtrosConsulta = null;

    @FindBy(id="EB_CONSULTA_DE_DEPOSITO_EXPRESS")
    WebElement vwe_btnFncOtrosExpress = null;

    @FindBy(id="EB_CONSULTA_SWIFT_ACK")
    WebElement vwe_btnFncOtrosSwift = null;

    @FindBy(id="EB_ADMINISTRADOR_DE_PROGRAMACIONES")
    WebElement vwe_btnFncOtrosProgramaciones = null;


    public void click_btnMenuPrincipalOtros(String tituloMenu) {
        try {
            vwe_btnMenuPrincipalOtros.click();
            Thread.sleep(2500);
            String textVwe = vwe_btnMenuPrincipalOtros.getText();
            Util.assert_contiene(vGral_menu+tituloMenu,  vGral_msjReporte, textVwe, tituloMenu, false, "N");
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            e.printStackTrace();
        }
    }

    public void click_btnFncOtrosFacturacion(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosFacturacion.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosFacturacion.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncOtrosBuro(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosBuro.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosBuro.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncOtrosScore(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosScore.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosScore.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncOtrosTransacciones(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosTransacciones.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosTransacciones.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncOtrosConsulta(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosConsulta.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosConsulta.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncOtrosExpress(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosExpress.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosExpress.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncOtrosSwift(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosSwift.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosSwift.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void click_btnFncOtrosProgramaciones(String tituloSeccion) {
        try {
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_btnFncOtrosProgramaciones.getText(), tituloSeccion, true, "N");
            Thread.sleep(vGral_sleep);
            vwe_btnFncOtrosProgramaciones.click();
        } catch (InterruptedException | NoSuchElementException | AssertionError e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    public void validate_EtiquetaMenu(String tituloSeccion) {
        try {
            WebElement vwe_lblTituloSeccion = Util.driver.findElement(By.xpath("//div[@class='titulo-seccion' and contains(., '"+ tituloSeccion + "')]"));
            Util.assert_contiene(vGral_menu, vGral_msjReporte, vwe_lblTituloSeccion.getText(), tituloSeccion, true, "N");
        } catch (NoSuchElementException e) {
            validate_Error(e.getMessage(), tituloSeccion);
        }
    }

    private void validate_Error(String error, String tituloSeccion) {
        System.out.println(vGral_err + tituloSeccion);
        System.out.println(error);
        Util.assert_contiene("Menu " + vGral_menu, "ERROR NO SE COMPROBO LA ETIQUETA DEL MENU", tituloSeccion, tituloSeccion, false, "C");
    }

}
